const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth");

router.post("/register", (request, response) => {
  userControllers
    .registerUser(request.body)
    .then((resultFromController) => response.send(resultFromController));
});

router.get("/allUsers", (request, response) => {
  userControllers
    .allUsers()
    .then((resultFromController) => response.send(resultFromController));
});

router.post("/checkEmail", (request, response) => {
  userControllers
    .checkEmailExist(request.body)
    .then((resultFromController) => response.send(resultFromController));
});

router.post("/login", (request, response) => {
  userControllers
    .loginUser(request.body)
    .then((resultFromController) => response.send(resultFromController));
});

// s38 Activity
router.post("/detailsv1", (request, response) => {
  userControllers
    .getProfileA(request.body)
    .then((resultFromController) => response.send(resultFromController));
});

router.post("/detailsv2", (request, response) => {
  userControllers
    .getProfileB(request.body)
    .then((resultFromController) => response.send(resultFromController));
});

router.get("/details", auth.verify, userControllers.getProfile);

router.post("/enroll", (request, response) => {
  userControllers
    .enroll(request.body)
    .then((resultFromController) => response.send(resultFromController));
});

module.exports = router;
