const User = require("../models/users");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const course = require("../models/course");

module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,

    /*
			// bcrypt - package for password hashing
			// hashSync - synchronously generate a hash
			// hash - asynchoursly generate a hash
		*/
    // hashing - converts a value to another value
    password: bcrypt.hashSync(reqBody.password, 10),
    /*
			// 10 = salt rounds
			// Salt rounds is proportional to hashing rounds, the higher the salt rounds, the more hashing rounds, the longer it takes to generate an output 
		*/
    mobileNo: reqBody.mobileNo,
  });

  return newUser.save().then((user, error) => {
    if (error) {
      return false;
    } else {
      return true;
    }
  });
};

module.exports.allUsers = () => {
  return User.find({}).then((result) => {
    return result;
  });
};

module.exports.checkEmailExist = (reqBody) => {
  return User.find({ email: reqBody.email }).then((result) => {
    if (result.length > 0) {
      return true;
    } else {
      return false;
    }
  });
};

module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result == null) {
      return false;
    } else {
      // compareSync is a bcrypt function to compare unhashed password to hashed password
      const isPasswordCorrect = bcrypt.compareSync(
        reqBody.password,
        result.password
      ); //true or false

      if (isPasswordCorrect) {
        // Let's give the user a token to a access features
        return { access: auth.createAccessToken(result) };
      } else {
        // If password does not match, else
        return false;
      }
    }
  });
};

// s38 Activity - userController.js
module.exports.getProfileA = (reqBody) => {
  return User.findById(reqBody._id).then((result, err) => {
    if (err) {
      return false;
    } else {
      result.password = "******";
      return result;
    }
  });
};

module.exports.getProfileB = (reqBody) => {
  return User.findOne({ _id: reqBody._id }).then((result, err) => {
    if (err) {
      return false;
    } else {
      result.password = "";
      return result;
    }
  });
};

module.exports.getProfile = (request, response) => {
  const userData = auth.decode(request.headers.authorization);

  return User.findById(userData.id).then((result) => {
    result.password = "*****";
    response.send(result);
  });
};

module.exports.enroll = async (request, response) => {
  const userData = auth.decode(request.headers.authorization);

  let courseName = await Course.findById(request.body.courseId).then(
    (result) => result.name
  );
  let newData = {
    userId: userData.id,
    email: userData.email,
    courseId: request.body.courseId,
    courseName: courseName,
  };

  console.log(newData);

  let isUserUpdated = await User.findById(newData.userId).then((user) => {
    User.enrollments.push({
      courseId: newData.courseId,
      courseName: newData.courseName,
    });

    return User.save()
      .then((result) => {
        console.log(result);
        return true;
      })
      .cath((error) => {
        console.log(error);
        return false;
      });
  });
  console.log(isUserUpdated);

  let isCourseUpdated = await Course.findById(data.courseId).then((course) => {
    course.enrollments.push({
      userId: data.userId,
      email: data.email,
    });
  });
  course.slots -= 1;

  return course
    .save()
    .then((result) => {
      console.log(result);
      return true;
    })
    .catch((error) => {
      console.log(error);
      return false;
    });
  console.log(isCourseUpdated);
};
