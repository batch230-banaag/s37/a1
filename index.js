const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
// port
const port = 4000;

// server
const app = express();
app.use(cors());

// to read json objects
app.use(express());
app.use(express.json());
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);
app.use(express.urlencoded({ extended: true }));

// Middlewares - allows to bridge our backend application to our fron end

// tp allow cross origin resource sharing

// to read foprms

// Connect to MongoDB database
mongoose.connect(
  "mongodb+srv://admin:admin@batch230.yvvrpsv.mongodb.net/S37?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

mongoose.connection.once("open", () =>
  console.log("Now connected to Banaag-Mongo DB Atlas")
);

app.listen(process.env.PORT || 4000, () => {
  console.log(`API is now online on port ${process.env.PORT || 4000}`);
});
